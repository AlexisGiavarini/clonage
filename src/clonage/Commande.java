package clonage;

/**
 * Classe representant l'entete d'une commande.
 */
class EnteteCommande implements Cloneable
{
    // nom du client qui a passe la commande
    private StringBuffer nomClient;
    // numero de la commande
    private int numeroCommande;

    EnteteCommande(String nomClient, int numero)
    {
        this.nomClient = new StringBuffer(nomClient);
        this.numeroCommande = numero;
    }

    void changeNomClient(String nomClient)
    {
        this.nomClient.replace(0,this.nomClient.length(),nomClient);
    }

    void changeNumeroCommande(int numeroCommande)
    {
        this.numeroCommande = numeroCommande;
    }

    public String toString()
    {
        return ("numero = " + this.numeroCommande
                + " effectu'ee par : " + this.nomClient);
    }
    
    public Object clone() throws CloneNotSupportedException
    {     
        EnteteCommande entCo = (EnteteCommande) super.clone();
        StringBuffer nomCl = new StringBuffer(this.nomClient);
        
        entCo.nomClient = nomCl;
        entCo.numeroCommande++;
        
        return entCo;
    }
}
/**
 * Classe representant un commande
 *
 */
public class Commande implements Cloneable
{
    // compteur du nombre de commandes creees
    private static int nombreCommandes = 0;
    private EnteteCommande entete;

    // le constructeur est inaccessible, pour creer des
    // commandes il faut utiliser la methode creerCommande
    private Commande(String nomClient, int numeroCommande)
    {
        this.entete = new EnteteCommande(nomClient, numeroCommande);
    }

    // permet la creation de commande
    static Commande creerCommande(String nomClient)
    {
        return new Commande(nomClient, ++nombreCommandes);
    }

    // permet de modifier le client d'une commande
    public void fixeClient(String nomClient)
    {
        this.entete.changeNomClient(nomClient);
    }

    // permet la copie de commandes
    public Object clone() throws CloneNotSupportedException
    {
        Commande co = (Commande) super.clone();
        co.entete = (EnteteCommande) this.entete.clone();
        nombreCommandes++;
        
        return co;
    }

    public String toString()
    {
        return ("Commande " + this.entete);
    }

    public static void main(String[] args) throws Exception
    {
        // création de deux commandes
        Commande cmd1 = Commande.creerCommande("Martin");
        Commande cmd2 = Commande.creerCommande("Durand"), cmd3;

        System.out.println(cmd1);
        System.out.println(cmd2);
        // duplication de la commande cmd2
        cmd3 = (Commande) cmd2.clone();
        // changement du client initial de cette commande
        cmd3.fixeClient("Dupont");

        System.out.println("Apres la copie de la commande");
        System.out.println(cmd2);
        System.out.println(cmd3);
    }
}
